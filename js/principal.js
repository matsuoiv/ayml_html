/*$(window).scroll(function(){

	var scroll=$(window).scrollTop();
		if(scroll>176){
			$(".link-up").fadeIn();
			$(".contenedor-menu").addClass("nav-activo");
		}
		else{ 
			$(".link-up").fadeOut();
			$(".contenedor-menu").removeClass("nav-activo");
		}
})

$(".link-up").click(function(){
	$("html,body").animate({scrollTop:0},750)
})*/

$(document).ready(function() {
	var scrollLink = $('.scroll');
	scrollLink.click(function(e){
		e.preventDefault();
		$('body,html').animate({
			scrollTop: $(this.hash).offset().top
		}, 1000 );
	})
})


$(window).scroll(function(){
	parallax();
})

function parallax(){
	if ($(window).width() > 1000) {
		var wScroll = $(window).scrollTop();
			$('.logo-grande').css('margin-top',
			350+(wScroll*0.25)+'px');
	} else{

	};
}


$(document).ready(function(){
	$('.text-bio').click(function(){
		$('.contenedor-bio').toggle(400);
		$('.text-bio').fadeToggle(400);
	})
})


$(document).ready(function(){
	$('.img-bio').click(function(){
		$('.contenedor-bio').toggle(400);
		$('.text-bio').fadeToggle(400);
	})
})

$(document).ready(function(){
	$('.contenedor-bio').click(function(){
		$('.text-bio').toggle(400);
		$('.contenedor-bio').fadeToggle(400);
	})
})

$(document).ready(function(){
	$('.slider-video').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		speed: 300,
		cssEase: 'linear',
		asNavFor: '.slider-imagenes',
	});
	$('.slider-imagenes').slick({
		centerMode: true,
		centerPadding: '80px',
		slidesToShow: 5,
		asNavFor: '.slider-video',
		focusOnSelect: true,
		arrows: false,
		responsive: [
			{
			breakpoint: 1000,
			settings: {
				slidesToShow: 3,
				centerPadding: '12%',
				centerMode: true,
				arrows: false,
				}
    		},
    		{
			breakpoint: 700,
			settings: {
				slidesToShow: 1,
				centerPadding: '32%',
				centerMode: true,
				arrows: false,
				}
    		},
    		{
			breakpoint: 500,
			settings: {
				slidesToShow: 1,
				centerMode: false,
				arrows: true,
				}
    		}
    	]
	});
	$('.slider-tienda').slick({
		centerMode: true,
		centerPadding: '110px',
		slidesToShow: 3,
		focusOnSelect: true,
		arrows: false,
		asNavFor: '.slider-textotienda',
		responsive: [
			{
			breakpoint: 1000,
			settings: {
				slidesToShow: 1,
				centerPadding: '33%',
				centerMode: true,
				arrows: false,
				}
    		},
    		{
			breakpoint: 700,
			settings: {
				slidesToShow: 1,
				centerPadding: '25%',
				centerMode: true,
				arrows: false,
				}
    		},
    		{
			breakpoint: 500,
			settings: {
				slidesToShow: 1,
				centerMode: false,
				arrows: true,
				}
    		}
    	]
	});
	$('.slider-textotienda').slick({
		centerMode: true,
		centerPadding: '110px',
		slidesToShow: 1,
		focusOnSelect: true,
		arrows: false,
		fade: true,
		speed: 300,
		asNavFor: '.slider-tienda',
	});
});

$(document).ready(function() {
	$(window).scroll(function() {
  	if($(document).scrollTop() > 124) {
    	$('.menu').addClass('shrink');
    }
    else {
    $('.menu').removeClass('shrink');
    }
  });
});

$(document).ready(function() {
	$(window).scroll(function() {
  	if($(document).scrollTop() > 124) {
    	$('.foot').addClass('footer');
    }
    else {
    $('.foot').removeClass('footer');
    }
  });
});


/////////////////////////////////////////////////////////


function openModal() {
  document.getElementById('myModal').style.display = "block";
}

function closeModal() {
  document.getElementById('myModal').style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}


/*
$(document).ready(function(){
	var feed = new Instafeed({
		get: 'tagged',
		tagName: 'chaclacayo',
		accessToken: '188176334.9cf7d21.25ff6dbeaede4f34ba772e439e5d7731',
	});
	feed.run();
})



$(function() {
    $( "#button" ).on( "click", function() {
      $( "#effect" ).addClass( "newClass", 1000, callback );
    });
 
    function callback() {
      setTimeout(function() {
        $( "#effect" ).removeClass( "newClass" );
      }, 1500 );
    }
  } );
*/